<?php
namespace app\modules\rabbitmq\controllers;

use yii\web\Controller;
use app\modules\rabbitmq\server\StaticFactoryServer;

class RabbitmqController extends Controller
{

    /**
     * @brief 生产者
     * @param type $name name defaultValue
     * @method POST/GET
     * @detail 接口描述
     * @return array
     * @throws Null
     */
    public function actionIndex()
    {
        $queueName = 'queuename';
        $exchange = 'queuename';
        $data = ['湛慧平','25','男'];
        $ConsumerClass = '\app\modules\rabbitmq\server\TestServer';
        $method = 'Test';
        $argv = ['data'=>$data,'consumerclass'=>$ConsumerClass,'method'=>$method];
        $RabbitmqFactory =  StaticFactoryServer::RabbitmqFactory();
        $RabbitmqFactory->rabbitProducer($queueName,$exchange,$argv);
        echo 'success';
    }


}
