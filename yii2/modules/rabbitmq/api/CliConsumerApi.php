<?php
namespace app\modules\rabbitmq\api;
use app\modules\rabbitmq\server\StaticFactoryServer;
/**
 * Created by PhpStorm.
 * Date: 2019/4/1
 * Time: 14:26
 */

class CliConsumerApi
{

    /**
     * @brief 消费者
     * @param type $name name defaultValue
     * @method POST/GET
     * @detail 接口描述
     * @return array
     * @throws Null
     */
    public static function ConsumerApi()
    {
        $queueName = 'queuename';
        $RabbitmqFactory =  StaticFactoryServer::RabbitmqFactory();
        $RabbitmqFactory->rabbitConsumer($queueName);
    }

    /**
     * 到具体类，具体方法
     * @param $consumerclass
     * @param $method
     * @param $data
     */
    public static function RabbitmaTransfer($consumerclass,$method,$data)
    {
        $consumerclass::$method($data);
    }


}