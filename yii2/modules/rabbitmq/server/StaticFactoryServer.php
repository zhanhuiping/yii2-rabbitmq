<?php
namespace app\modules\rabbitmq\server;
use app\modules\rabbitmq\server\RabbitObserver;
/**
 * Created by PhpStorm.
 * Date: 2019/4/1
 * Time: 14:26
 */

class StaticFactoryServer
{

    /**
     * @return \app\modules\rabbitmq\server\RabbitObserver
     *  工厂方法，实例化Rabbitmq类
     */
    public static function RabbitmqFactory()
    {
           return  (new RabbitObserver());
    }

}