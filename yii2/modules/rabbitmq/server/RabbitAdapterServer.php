<?php
namespace app\modules\rabbitmq\server;

//目标角色  
interface RabbitAdapterServer {
    public function rabbitConsumer($queueName);
    public function rabbitProducer($queueName,$exchange,$argv);
}

