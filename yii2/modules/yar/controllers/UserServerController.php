<?php
namespace app\modules\yar\controllers;

use yii\web\Controller;

class UserServerController extends Controller
{

    /**
     * 架构函数
     * @access public
     */
    public function init()
    {
        parent::init();

        //判断扩展是否存在
        if (!extension_loaded('yar')) {
            throw new \Exception('not support yar');
        }

        //实例化Yar_Server
        $server = new \Yar_Server($this);
        // 启动server
        $server->handle();
        exit;
    }

    /**
     * 魔术方法 有不存在的操作的时候执行
     * @access public
     * @param string $method 方法名
     * @param array $args 参数
     * @return mixed
     */
    public function __call($method, $args){}

    public function actionIndex()
    {

    }

    public function actionTest()
    {
       return 'test';
    }





}
