<?php
namespace app\modules\yar\controllers;

use yii\web\Controller;

class UserController
{


    /**
     * @brief 接口名称
     * @param type $name name defaultValue
     * @method POST/GET
     * @detail 测试接口
     * @return array
     * @throws Null
     * @return string
     */
    public function actionYar()
    {
        return 'success yarMethod';
    }


    /**
     * @brief 接口名称
     * @param type $name name defaultValue
     * @method POST/GET
     * @detail 测试接口
     * @return array
     * @throws Null
     * @return string
     */
    public function actionTest()
    {
        return 'success Test';
    }




}
