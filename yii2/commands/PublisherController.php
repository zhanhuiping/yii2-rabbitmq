<?php
namespace app\commands;

use yii\console\Controller;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

/*
*@publisher 生产者
*/
class PublisherController extends Controller
{

    public $exchange = 'task_queue';
    public $queue = 'task_queue';


    public function actionPublisher($argv)
    {

       $connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest', '/');
       $channel = $connection->channel();


       $channel->queue_declare($this->queue, false, true, false, false);


       $channel->exchange_declare($this->exchange, 'direct', false, true, false);

       $channel->queue_bind($this->queue, $this->exchange);

       $argv = ['',$argv];
       $messageBody = implode(' ', array_slice($argv, 1));
       $message = new AMQPMessage($messageBody, array('content_type' => 'text/plain', 'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT));
       $channel->basic_publish($message, $this->exchange);

       $channel->close();
       $connection->close();
    }
}
