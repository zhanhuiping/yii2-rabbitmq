<?php
namespace app\commands;

use yii\console\Controller;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use app\modules\rabbitmq\api\CliConsumerApi;

/*
*Consumer消费者
*/
class ConsumerController extends Controller
{


    public function actionConsumer()
    {
        $connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
        $channel = $connection->channel();

        $channel->queue_declare('task_queue', false, true, false, false);

        $callback = function ($msg) {
            echo $msg->body;
            $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
        };

        $channel->basic_qos(null, 1, null);
        $channel->basic_consume('task_queue', '', false, false, false, false, $callback);

        while (count($channel->callbacks)) {
            $channel->wait();
        }

        $channel->close();
        $connection->close();
    }

    public function actionConsumerTest()
    {

        $CliConsumerApi = new CliConsumerApi();
        $CliConsumerApi::ConsumerApi();

    }
        

}
